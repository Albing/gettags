#!/bin/bash

ADDR=$(pwd)
FILELOC=$ADDR/tagList.txt

echo "This could take a while."

#Note, using tee without the -a option (like one greater-than) to specify starting the file from fresh
echo "*****List of Tags*****" >> $FILELOC

for VAL in $(find . -name ".git")
do
    NOGIT="${VAL%.git}"
    NODOT="${NOGIT:1}"
    echo "Location:  $NOGIT" | tee -a $FILELOC
    REPOLOCATION="$ADDR$NODOT"
    cd $REPOLOCATION
    TAG=$(git tag --points-at HEAD)
    BRANCH=$(git branch --show-current)
    COMMIT_ID_SHORT=$(git rev-parse --short HEAD)
    
    if [[ -n $BRANCH ]]
    #then
        #echo "Branch:    No Branch!" >> $FILELOC
    #else
    then
        echo "Branch:    $BRANCH" >> $FILELOC
    fi
    
    if [[ -n $TAG ]]
    #then
        #echo "tag:       No Tag!" >> $FILELOC
    #else
    then
        echo "Tag:       $TAG" >> $FILELOC
    fi
    echo "ID:        $COMMIT_ID_SHORT" >> $FILELOC
    #echo >> $FILELOC
done

echo "Done!"
